# git-update-branch

Simple git command for updating branches from a remote in an easy way.

# Usage

If you omit the branch name (`master` in the examples below), then the branch to use defaults to your current branch (`git branch --show-current`).

```bash
# pull + --ff-only
git update-branch master
# pull
git update-branch -m master
# fetch + reset --hard
git update-branch -fH master
```

# Installation

# Makefile

```bash
# Copy file using `install` command
make PREFIX=/usr/local install
# Use symlink
make PREFIX=/usr/local INSTALL_LINK=1 install
```

# Manual

Simply copy `git-update-branch` to somewhere on your `PATH` (recommended: `/usr/local/bin` if installing manually. `/usr/bin` if creating a package)
