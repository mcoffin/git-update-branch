#!/bin/bash
repo_dir=testrepo
clone_url="https://gitlab.com/mcoffin/git-update-branch.git"
while getopts ":d:r:" opt; do
	case ${opt} in
		r)
			clone_url="$OPTARG"
			;;
		d)
			repo_dir="$OPTARG"
			;;
	esac
done
shift $((OPTIND - 1))

set -e

git clone "$clone_url" "$repo_dir"
pushd "$repo_dir"
git update-branch "$@"
popd
