.DEFAULT_GOAL := install

PREFIX ?= /usr/local
DESTDIR ?= /
INSTALL_LINK ?=

$(DESTDIR)/bin:
	[ -d "$@" ] || mkdir -p "$@"

install: git-update-branch $(DESTDIR)
ifeq ($(INSTALL_LINK),)
	install -Dm 0755 -t $(DESTDIR)/$(PREFIX)/bin $<
else
	ln -sf "$(shell pwd)"/git-update-branch "$(DESTDIR)/$(PREFIX)/bin/git-update-branch"
endif

.PHONY: install
